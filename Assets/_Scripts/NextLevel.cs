﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {
    public float TimeToChange = 20f;
    private int SceneEncours;
	
    void Start ()
    {
        SceneEncours = SceneManager.GetActiveScene().buildIndex;
    }
    
    
    void Update () {
        if (Time.timeSinceLevelLoad > TimeToChange)
        {
            if (SceneEncours < 3)
            {
                SceneManager.LoadScene(SceneEncours + 1);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
