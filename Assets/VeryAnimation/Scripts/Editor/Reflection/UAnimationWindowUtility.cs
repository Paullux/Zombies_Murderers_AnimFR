﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace VeryAnimation
{
    public class UAnimationWindowUtility
    {
        private MethodInfo mi_IsNodeLeftOverCurve;
        private MethodInfo mi_CreateNewClipAtPath;
        private MethodInfo mi_CurveSelectionToAnimationWindowKeyframe;

        public UAnimationWindowUtility()
        {
            var asmUnityEditor = Assembly.LoadFrom(InternalEditorUtility.GetEditorAssemblyPath());
            var animationWindowUtilityType = asmUnityEditor.GetType("UnityEditorInternal.AnimationWindowUtility");
            Assert.IsNotNull(mi_IsNodeLeftOverCurve = animationWindowUtilityType.GetMethod("IsNodeLeftOverCurve", BindingFlags.Public | BindingFlags.Static));
            Assert.IsNotNull(mi_CreateNewClipAtPath = animationWindowUtilityType.GetMethod("CreateNewClipAtPath", BindingFlags.NonPublic | BindingFlags.Static));
            Assert.IsNotNull(mi_CurveSelectionToAnimationWindowKeyframe = animationWindowUtilityType.GetMethod("CurveSelectionToAnimationWindowKeyframe", BindingFlags.Public | BindingFlags.Static));
        }

        public bool IsNodeLeftOverCurve(object node)
        {
            return (bool)mi_IsNodeLeftOverCurve.Invoke(null, new object[] { node });
        }

        public AnimationClip CreateNewClipAtPath(string clipPath)
        {
            return mi_CreateNewClipAtPath.Invoke(null, new object[] { clipPath }) as AnimationClip;
        }

        public object CurveSelectionToAnimationWindowKeyframe(object curveSelection, object allCurves)
        {
            return mi_CurveSelectionToAnimationWindowKeyframe.Invoke(null, new object[] { curveSelection, allCurves });
        }
    }
}
