﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Reflection;

namespace VeryAnimation
{
    public class UDisc
    {
        private Action<object, Vector2> dg_set_s_StartMousePosition;
        private Func<object, float> dg_get_s_RotationDist;

        public UDisc()
        {
            var asmUnityEditor = Assembly.LoadFrom(InternalEditorUtility.GetEditorAssemblyPath());
            var discType = asmUnityEditor.GetType("UnityEditorInternal.Disc");
            Assert.IsNotNull(dg_set_s_StartMousePosition = EditorCommon.CreateSetFieldDelegate<Vector2>(discType.GetField("s_StartMousePosition", BindingFlags.NonPublic | BindingFlags.Static)));
            Assert.IsNotNull(dg_get_s_RotationDist = EditorCommon.CreateGetFieldDelegate<float>(discType.GetField("s_RotationDist", BindingFlags.NonPublic | BindingFlags.Static)));
        }

        public void ResetStartMousePosition()
        {
            dg_set_s_StartMousePosition(null, Event.current.mousePosition);
        }
        
        public float GetRotationDist()
        {
            return dg_get_s_RotationDist(null);
        }
    }
}
